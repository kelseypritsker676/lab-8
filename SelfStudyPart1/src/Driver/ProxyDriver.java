/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Driver;
import Part1Packages.ProxyComplexObject;

/**
 *
 * @author kelsey.pritsker676
 */
public class ProxyDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ProxyComplexObject obj = new ProxyComplexObject();
        obj.useRealObject();
    }
}
