/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Driver;

import Part1Packages.SingletonExample;

/**
 *
 * @author kelsey.pritsker676
 */
public class SingletonDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Was Singleton pattern used correctly?");
        SingletonExample ex1 = SingletonExample.getExample();
        SingletonExample ex2 = SingletonExample.getExample();
        
        if(ex1==ex2)
            System.out.println("yes");
        else
            System.out.println("no");
    }
}
