/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Part1Packages;

/**
 *
 * @author kelsey.pritsker676
 */
public class ProxyComplexObject implements ProxyInterface{
    RealComplexObject compObj = new RealComplexObject();
    
    @Override
    public void useRealObject(){
        compObj.doComplexStuff();
    }
    
}
